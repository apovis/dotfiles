#
# ~/.bashrc
#

# date: 2024.06.04

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#PS1='\[\033[1;32m\]\u\[\033[0m\]@\[\033[0;32m\]\h\[\033[0m\] \W]\$ '
#PS1='\[\033[1;32m\]\u\[\033[0m\]@\[\033[0;32m\]\h\[\033[0m\] \[\033[1;34m\]\w\[\033[0m\]]\$ '
#PS1='\[\033[38;5;209m\]┌──[\[\033[1;32m\]\u\[\033[0m\]@\[\033[0;32m\]\h\[\033[0m\] \[\033[1;34m\]\w\[\033[0m\]\[\033[38;5;209m\]]\n\[\033[38;5;209m\]└─\[\033[0m\] $ '
PS1='\[\033[38;5;209m\][\[\033[1;32m\]\u\[\033[0m\]@\[\033[0;32m\]\h\[\033[0m\] \[\033[1;34m\]\W\[\033[0m\]\[\033[38;5;209m\]]\[\033[0m\] $ '
PATH="${PATH}:/usr/bin/vendor_perl/"

## english
export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_PAPER="en_US.UTF-8"
export LC_NAME="en_US.UTF-8"
export LC_ADDRESS="en_US.UTF-8"
export LC_TELEPHONE="en_US.UTF-8"
export LC_MEASUREMENT="en_US.UTF-8"
export LC_IDENTIFICATION="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"

# bash completions
[ -f /usr/share/bash-completion/bash_completion ] && source /usr/share/bash-completion/bash_completion

export XDG_CONFIG_DIR="/home/apovis/.config"
export XDG_CONFIG_HOME="/home/apovis/.config"
export XDG_RUNTIME_DIR="/run/user/$(id -u)"
export XDG_CURRENT_DESKTOP="sway"

# histsize
export HISTSIZE=10000000
export HISTTIMEFORMAT='%B %Y %A %H:%M:%S | '
export HISTCONTROL=ignoredups
# save multi-line commands in history as single line
export HISTIGNORE='ls:bg:fg:history'
export PROMPT_COMMAND='history -a'

# forward search
stty -ixon

### SHOPT
# "." in the results of pathname expansion
#shopt -s dotglob
# enable history
shopt -s cmdhist 
# append to history
shopt -s histappend
# change to named directory
#shopt -s autocd 
# autocorrects cd misspellings
#shopt -s cdspell 
# expand aliases
#shopt -s expand_aliases 
# checks term size when bash regains control
#shopt -s checkwinsize 
# set gnu std error format
#shopt -s gnu_errfmt
#ignore upper and lowercase when TAB completion
#bind "set completion-ignore-case on"

alias ls='ls --color=auto'
alias ll='ls -lh --color=auto'
alias la='ls -lah --color=auto'
alias lv='ls -hrtl --color=auto'
alias lav='ls -ahrtl --color=auto'

export EDITOR="nvim"
export VISUAL="nvim"
export PAGER="less"
export MANPAGER="less"
complete -cf doas
export TERM="xterm-256color"

alias nv='nvim'
alias vim='nvim'

REFL() {
  doas reflector -n 12 -a 8 -c DE -f 15 -p https --sort rate --save /etc/pacman.d/mirrorlist
}

IWD_UP() {
 doas systemctl start iwd.service
}

IWD_DOWN() {
 doas systemctl stop iwd.service
}

BZU() {
  doas systemctl start bluetooth
}

BZD() {
  doas systemctl stop bluetooth
}

CUPS_UP() {
  doas systemctl start cups
}

CUPS_DOWN() {
  doas systemctl stop cups
}

CUPS_RE() {
  doas systemctl restart cups
}

POD_CTN_RM(){
  podman container rm $(podman ps -aq | xargs)
}

POD_CTN_STOP(){
  podman container stop $(podman ps -a | xargs)
}

POD_RMI(){
  podman rmi $(podman images -aq | xargs)
}

BCR(){
  buildah rm $(buildah containers | tail +2 | awk '{print $1}' | xargs)
}

#updates
UPPY() {
  doas pacman -Syu
  doas pkgfile --update
  doas systemctl daemon-reload
  doas pacman -Sc --noconfirm
}

# only needed IF grub is used!
#NK () 
#{ 
#    doas grub-mkconfig -o /boot/grub/grub.cfg;
#    doas grub-install --target=x86_64-efi --efi-directory=/efi --boot-directory=/boot --bootloader-id=archlinux --recheck
#}

INS() {
  doas pacman -S --needed --noconfirm "$@"
}

QUERY() {
  pacman -Ss $1
}

GADD() {
	gpg --recv-keys $1
	gpg --edit-key $1 trust quit
}

#pwd gen
APG() {
	apg -a 0 -M SNCL -t -n 15 -m 16 -x 28 -s
}

APG1() {
	apg -a 1 -M SNCL -t -n 16 -m 16 -x 28 -s
}

RS() {
  rsync -aP --partial $1 $2
}

# grep with color
#export GREP_OPTIONS='--color=auto'
alias grep='grep --color=auto'

# tar aliases
#extract
alias txz='tar -xvzf'
alias txj='tar -xvjf'
alias txJ='tar -xvJf'

#create
alias tcz='tar -cvzf'
alias tcj='tar -cvjf'
alias tcJ='tar -cvJf'

P7Z() {
  7z a -t7z -mhe=on -mx=9 -m0=lzma2 "$1" "$2"
}

# clear and kill keychain
KK() {
	keychain --clear
	keychain -k all
}

TN() {
  tmux new -s $1
}

TA() {
  tmux a -t $1
}

TL() {
  tmux list-sessions
}

# weather
WOB() {
	curl -s 'https://wttr.in/Oberreifenberg,Germany'
}

WBL() {
	curl -s 'https://wttr.in/Bleckede,Germany'
}

WOL_VIRTBOX() {
  wol 10:27:f5:26:e5:58
  wol 18:c0:4d:66:ad:29
}

ANSIBLE_LATEST() {
  source ~/.py_envs/ansible_latest/bin/activate
}

UPDATE_PIP() {
  pip install -U $(pip list | tail -n+3 | awk '{print $1}' | xargs)
}

TPW() {
  for i in {1..5}
  do 
    talosctl shutdown -n piclusrv0${i} -e tck8s --talosconfig talosconfig 
  done
}

TPC() {
  for i in {1..3}
  do 
    talosctl shutdown -n rlk8s0${i} -e tck8s --talosconfig talosconfig 
  done
}

RBP() {
  readarray -t blub01 < <(kubectl get pods -A | grep -Ei '(Error|ContainerStatusUnknown|Completed)' | awk '{print $1,$2 }')
  for i in ${!blub01[@]}; do kubectl delete pod -n ${blub01[${i}]};done
}

#without gpg
eval $(keychain --eval -Q --nogui --confhost --agents ssh,gpg ${HOME}/.ssh/id_ed25519 ${HOME}/.ssh/id_ed25519-sk 22F325B7)
#eval $(keychain --eval -Q --nogui --confirm --confhost --agents ssh,gpg id_ed25519-sk id_ed25519 id_ed25519-swissit 22F325B7)

# kubectl_aliases
[ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases
