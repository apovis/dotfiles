include ./theme.conf

: Fonts {{{

# font_family      SourceCodePro-Regular
# bold_font        SourceCodePro-Bold
# italic_font      SourceCodePro-It
# bold_italic_font SourceCodePro-BoldIt

font_family      SourceCodeVF
bold_font        auto
italic_font      auto
bold_italic_font auto

font_size 12.0

: }}}

: Scrollback {{{

scrollback_lines 20000

scrollback_pager less --chop-long-lines --RAW-CONTROL-CHARS +INPUT_LINE_NUMBER

wheel_scroll_multiplier 5.0

wheel_scroll_min_lines 1

touch_scroll_multiplier 1.0

: }}}

: Performance tuning {{{

repaint_delay 10

input_delay 3

sync_to_monitor yes

: }}}

: Terminal bell {{{

enable_audio_bell no

visual_bell_duration 0.9

: }}}

: Tab bar {{{

tab_bar_edge top

tab_bar_margin_width 0.2

tab_bar_margin_height 0.1 0.1

tab_bar_style fade

tab_bar_align left

tab_bar_min_tabs 2

tab_switch_strategy previous

tab_fade 0.25 0.5 0.75 1

tab_separator "<|>"

tab_title_max_length 0

tab_title_template "{fmt.fg.red}{bell_symbol}{activity_symbol}{fmt.fg.tab}{title}"

: }}}

: Tab management {{{

: Next tab

 map kitty_mod+right next_tab
 map shift+cmd+]     next_tab
 map ctrl+tab        next_tab

: Previous tab

 map kitty_mod+left previous_tab
 map shift+cmd+[    previous_tab
 map ctrl+shift+tab previous_tab

: New tab

 map kitty_mod+t new_tab
 map cmd+t       new_tab

: Close tab

 map kitty_mod+q close_tab
 map cmd+w       close_tab

: Close OS window

 map shift+cmd+w close_os_window

: Move tab forward

 map kitty_mod+. move_tab_forward

: Move tab backward

 map kitty_mod+, move_tab_backward

: Set tab title

 map kitty_mod+alt+t set_tab_title
 map shift+cmd+i     set_tab_title


: You can also create shortcuts to go to specific tabs, with 1 being
: the first tab, 2 the second tab and -1 being the previously active
: tab, and any number larger than the last tab being the last tab::

:     map ctrl+alt+1 goto_tab 1
:     map ctrl+alt+2 goto_tab 2

: Just as with new_window above, you can also pass the name of
: arbitrary commands to run when using new_tab and new_tab_with_cwd.
: Finally, if you want the new tab to open next to the current tab
: rather than at the end of the tabs list, use::

:     map ctrl+t new_tab !neighbor [optional cmd to run]
: }}}

: Font sizes {{{

: You can change the font size for all top-level kitty OS windows at
: a time or only the current one.

: Increase font size

 map kitty_mod+equal  change_font_size all +2.0
 map kitty_mod+plus   change_font_size all +2.0
 map kitty_mod+kp_add change_font_size all +2.0
 map cmd+plus         change_font_size all +2.0
 map cmd+equal        change_font_size all +2.0
 map shift+cmd+equal  change_font_size all +2.0

: Decrease font size

 map kitty_mod+minus       change_font_size all -2.0
 map kitty_mod+kp_subtract change_font_size all -2.0
 map cmd+minus             change_font_size all -2.0
 map shift+cmd+minus       change_font_size all -2.0

: Reset font size

 map kitty_mod+backspace change_font_size all 0
 map cmd+0               change_font_size all 0


: To setup shortcuts for specific font sizes::

:     map kitty_mod+f6 change_font_size all 10.0

: To setup shortcuts to change only the current OS window's font
: size::

:     map kitty_mod+f6 change_font_size current 10.0
: }}}
